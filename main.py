#!/usr/bin/python3

from optparse import OptionParser
from linux import linuxVectorEmbedding




parser = OptionParser()
parser.add_option("-i",
                  "--input",
                  dest="input",
                  type="string",
                  help="path of networkFile",
                  default="")
parser.add_option("-b",
                  "--binary",
                  dest="binary",
                  type="int",
                  help="choice for result in txt(0) binary(1) both(2) format",
                  default=0)
parser.add_option("-d",
                  "--dimension",
                  dest="dimension",
                  type="int",
                  help="half of result vector dimension",
                  default=100)
parser.add_option("-l",
                  "--learningRate",
                  dest="learningRate",
                  type="float",
                  help="learningRate for EmbeddingAlgorithm",
                  default=0.05)
parser.add_option("-t",
                  "--threads",
                  dest="threads",
                  type="int",
                  help="number of working threads",
                  default=8)

(opts, args) = parser.parse_args()

if __name__ == '__main__':
    linuxVectorEmbedding(opts)