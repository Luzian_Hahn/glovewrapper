import subprocess
import sys


def linuxVectorEmbedding(opts):
    dir = 'GloVe-1.2/'
    buildDir = 'build/'
    verbose = '2'
    vocabFile = 'vocab.txt'
    vocabMinCount = '5'
    memory = '4.0'
    windowSize = '15'
    cooccurrenceFile = 'cooccurrence.bin'
    cooccurrenceShuffleFile = 'cooccurrence.shuf.bin'
    vectorFile = 'vectors'
    xMax = '10'
    maxIter = '15'

    try:
        inputFile = open(opts.input, 'r')
        inputFile.seek(0)
        if not(inputFile.read(1)):
            print('InputFile is Empty! Program canceled!')
            sys.exit(1)
        inputFile.close()
    except:
        print('Error! Could not open InputFile!')
        print(sys.exc_info())
        sys.exit(2)

    try:
        subprocess.call('make')

        subprocess.call(['./'+dir+buildDir+"vocab_count", '-min-count', vocabMinCount, '-verbose', verbose, '<', opts.input, '>', vocabFile])
        subprocess.call(['./'+dir+buildDir+"cooccur", '-memory', memory, '-verbose', verbose, '-vocab-file', vocabFile,
                         '-window-size', windowSize, '<', opts.input, '>', cooccurrenceFile])
        subprocess.call(['./'+dir+buildDir+"shuffle", 'memory', memory, '-verbose', verbose, '<', cooccurrenceFile,
                         '>', cooccurrenceShuffleFile])
        subprocess.call(['./'+dir+buildDir+'glove', '-save-file', vectorFile, '-threads', str(opts.threads),
                         'input-file', cooccurrenceShuffleFile, '-x-max', xMax, '-iter', maxIter,
                         '-vector-size', str(opts.dimension), '-binary', str(opts.binary), '-vocab-file', vocabFile,
                         '-verbose', verbose])

        subprocess.call(['make','clean'])
    except:
        print(sys.exc_info())
        sys.exit(3)

        
