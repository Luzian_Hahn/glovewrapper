#!/usr/bin/python3

import pandas as pd

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-i",
                  "--input",
                  dest="input",
                  type="string",
                  help="path of tagFile",
                  default="")

parser.add_option("-f",
                  "--filter",
                  dest="filter",
                  type="string",
                  help="filter result from this user",
                  default="")

(opts, args) = parser.parse_args()

elements = pd.read_csv(opts.input, sep='\t', names=['tag','user','link'], dtype={'tag':str, 'user':str, 'link':str})
print("completed reading tsv-File\n")


filtered = elements[elements['user'] != opts.filter]
print("completed filtering tsv-File for: "+opts.filter+'\n')

links = {}
for link, tag in zip(filtered['link'],filtered['tag']):
  if link in links:
    links.update({link:links.get(link)+[tag]})
  else:
    links.update({link:[tag]})
print("completed mapping tags to links\n")

edges = {}
for link, tags in links.items():
  for tag1 in tags:
    for tag2 in tags:
      if tag1 != tag2:
        if (str(tag1)+'\t'+str(tag2)) in edges:
          edges.update({(str(tag1)+'\t'+str(tag2)):edges.get((str(tag1)+'\t'+str(tag2)))+1})
        else:
          edges.update({(str(tag1)+'\t'+str(tag2)):1})
print("completed creating edges\n")

file = open("edgeFile", 'w')
for edge,value in edges.items():
  file.write(edge+'\t'+str(value)+'\n')
print("finished successfully\n")


