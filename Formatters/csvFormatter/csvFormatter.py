#!/usr/bin/python3

import pandas as pd
from optparse import OptionParser



parser = OptionParser()
parser.add_option("-i",
                  "--input",
                  dest="input",
                  type="string",
                  help="path of csvFile",
                  default="")


(opts, args) = parser.parse_args()


iterator = pd.read_csv(opts.input, delimiter='\t', iterator=True, dtype={'prev_id':str, 'curr_id':str,'n':str, 'prev_title':str, 'curr_title':str, 'type':str}, lineterminator='\n', quoting=3)
data = pd.concat([chunk[chunk['type'] == 'link'] for chunk in iterator])

print('finished reading input')

data.to_csv("formattedcsvEdgeFile", sep='\t', columns=['prev_id', 'curr_id', 'n'], index=False, header=False)


