//
// Created by grave on 16.02.17.
//
#include <iostream>
#include <mutex>
#include <thread>
#include <vector>
#include <fstream>
#include <map>
#include <set>
#include <algorithm>
#include <sstream>

#ifndef MULTITHREADING_PATHFORMATTER_H
#define MULTITHREADING_PATHFORMATTER_H


std::mutex locker;
void readPaths(int threadID, int begin, int end, std::set<std::string> &edges, std::vector<std::string> &lines, std::map<std::string, int> &weights);

class PathFormatter {

public:
    PathFormatter(std::string pathOfFile, int numberOfThreads = 4, std::string pathOfResultFile = "formattedPathFile");
    void formatPathFile();

private:
    int numberOfThreads;
    std::string pathOfFile;
    std::string pathOfResultFile;


};


#endif //MULTITHREADING_PATHFORMATTER_H
