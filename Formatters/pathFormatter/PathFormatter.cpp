//
// Created by grave on 16.02.17.
//

#include "PathFormatter.h"

PathFormatter::PathFormatter(std::string inputPathOfFile, int inputNumberOfThreads, std::string inputPathOfResultFile)
:   pathOfFile(inputPathOfFile)
,   numberOfThreads(inputNumberOfThreads)
,   pathOfResultFile(inputPathOfResultFile)
{}


void PathFormatter::formatPathFile()
{

    std::thread threads[numberOfThreads];
    std::map<std::string,int> weightsPerThread[numberOfThreads];
    std::set<std::string> edges;
    std::vector<std::string> lines;
    std::fstream pathFile(pathOfFile);

    std::string tmp;
    while(std::getline(pathFile, tmp))
    {
        lines.insert(lines.end(),tmp);
    }
    pathFile.close();
    

    for(int i = 0; i < numberOfThreads;i++)
    {
	int start = (int)((float)i/(float)numberOfThreads*lines.size());
	int end = (int)((float)(i+1)/(float)numberOfThreads*lines.size());
	std::cout<<"debug: starting thread "<<i<<" with parameters: begin "<<start<<" end "<<end<<std::endl;
        threads[i] = std::thread(readPaths, i, start, end, std::ref(edges), std::ref(lines), std::ref(weightsPerThread[i]));
    }
    for(auto & thread : threads)
    {
        thread.join();
    }


    std::ofstream resultFile;
    resultFile.open(pathOfResultFile);
    int edgesCounter = 0;
    float progress;
    for(auto it = edges.begin(); it != edges.end(); it++)
    {
	int sum = 0;
	for(int i = 0; i < numberOfThreads; i++)
	{
	    auto tmpWeight = weightsPerThread[i].find(*it);
	    if(tmpWeight != weightsPerThread[i].end())
		sum += tmpWeight->second;
	}
        resultFile<<*it<<"\t"<<sum<<std::endl;

	if(edgesCounter % 50 == 0)
	    progress  = (float)edgesCounter/(float)edges.size()*100;
	    printf("%cmerging results... Progress: %.3lf%%", 13, progress);
	    fflush(stdout);
        edgesCounter++;
    }
    resultFile.close();
    std::cout<<std::endl;

}

void readPaths(int threadID, int begin, int end, std::set<std::string> &edges, std::vector<std::string> &lines, std::map<std::string, int> &weights)
{
    for(int currentPosition = begin; currentPosition < end; currentPosition++)
    {
        //mutex begin
        std::string line = lines.at(currentPosition);
        //mutex end
        std::stringstream ss(line);
        std::string tmp1, tmp2;
        getline(ss, tmp1, '\t');
        while(getline(ss, tmp2, '\t'))
        {
            locker.lock();
	    edges.insert(tmp1+"\t"+tmp2);
	    locker.unlock();
	    auto weight = weights.find(tmp1+"\t"+tmp2);
            if(weight == weights.end())
            {
                weights.insert(std::pair<std::string, int>(tmp1+"\t"+tmp2, 1));
            }
            else
            {
                (weight->second)++;
            }
	    tmp1 = tmp2;
        }
	float progress;
	if(currentPosition % 50 == 0)
	    progress  = (float)(currentPosition-begin)/(float)(end-begin)*100;
	    printf("%cthread %d Progress: %.3lf%%", 13, threadID, progress);
	    fflush(stdout);
    }
    std::cout<<std::endl;
}
