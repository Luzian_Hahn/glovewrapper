#include <iostream>
#include "PathFormatter.cpp"

int main(int argc, char *argv[])
{
	int threadCount = 4;
	std::string pathOfResultFile = "formattedPathFile";
	
	if (argc == 1) 
	{
		std::cout<<"Error! No File passed!"<<std::endl;
		return 1;
	}
	else 
	{
		if (argc >= 3) 
		{
			std::stringstream converter(argv[2]);
			if(!(converter >> threadCount)) threadCount = 4;
			if(argc >= 4) pathOfResultFile = argv[3];
		}
	}
	std::string pathOfInputFile(argv[1]);
	PathFormatter formatter(pathOfInputFile, threadCount, pathOfResultFile);
	formatter.formatPathFile();
	
	return 0;
}
