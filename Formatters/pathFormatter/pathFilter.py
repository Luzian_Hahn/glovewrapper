#!/usr/bin/python3
import pandas as pd
import re


fileIterator = pd.read_csv('paths_finished.tsv', sep='\t', usecols=[3], comment='#', header=0, names=['path'] ,dtype={'path':str})
wikispeediaPathsFile = open('pathsWikispeedia', 'w')

for chunk in fileIterator['path']:
	split = re.split(';[<;]*', chunk.strip())
	for i in range(0,split.__len__()):
		wikispeediaPathsFile.write(split[i])
		if(i != split.__len__()-1):
			wikispeediaPathsFile.write('\t')
		else:
			wikispeediaPathsFile.write('\n')

fileIterator = pd.read_csv('paths_unfinished.tsv', sep='\t', usecols=[3], comment='#', header=0, names=['path'] ,dtype={'path':str})

for chunk in fileIterator['path']:
	split = re.split(';[<;]*', chunk.strip())
	for i in range(0,split.__len__()):
		wikispeediaPathsFile.write(split[i])
		if(i != split.__len__()-1):
			wikispeediaPathsFile.write('\t')
		else:
			wikispeediaPathsFile.write('\n')

wikispeediaPathsFile.close()
