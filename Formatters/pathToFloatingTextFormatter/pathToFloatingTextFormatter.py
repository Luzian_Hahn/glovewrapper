#!/usr/bin/python3

import re

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-i",
                  "--input",
                  dest="input",
                  type="string",
                  help="path of pathFile separated with tabs",
                  default="")

(opts, args) = parser.parse_args()

inputFile = open(opts.input, 'r')
outputFile = open(opts.input+'FloatingText','w')

for line in inputFile.readlines():
  split = re.split('\t',line.strip())
  for word in split:
    outputFile.write(word+' ')


