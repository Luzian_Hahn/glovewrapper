//
// Created by grave on 16.02.17.
//
#include <iostream>
#include <string>
#include <thread>
#include <map>
#include <vector>
#include <set>
#include <algorithm>
#include <mutex>
#include <sstream>

std::mutex locker;

void readLinks(int threadID, int begin, int end, std::vector<std::string> &lines,
               std::map<std::string, std::set<std::string>> &linksMap, std::vector<std::string> &links)
{
    for(int currentPosition = begin; currentPosition < end; currentPosition++)
    {
        std::string line = lines.at(currentPosition);
        std::string user, tag, link;
        std::stringstream ss(line);

        //std::set<std::string> *tmpSet;
        ss>>tag>>user>>link;
        if(std::find(links.begin(),links.end(), link) != links.end())
        {
            locker.lock();
            links.insert(links.end(), link);
            locker.unlock();
        }
        std::set<std::string> *tmpSet;
        if(linksMap.find(link) != links.end())
        {
            tmpSet = &(linksMap.find(link)->second);
            locker.lock();
            tmpSet->insert(tag);
            locker.unlock();
        }
        else
        {
            tmpSet = new std::set<std::string>();
            locker.lock();
            tmpSet->insert(tag);
            linksMap.insert(std::pair<std::string, std::set<std::string>>(link, *tmpSet));
            locker.unlock();
        }
        float progress;
        if(currentPosition % 50 == 0)
            progress  = (float)(currentPosition-begin)/(float)(end-begin)*100;
        printf("%cthread %d Progress readInFile: %.3lf%%", 13, threadID, progress);
        fflush(stdout);

    }
}

void constructEdges(int threadID, int begin, int end, std::map<std::string, std::set<std::string>> &linksMap, std::vector<std::string> &links)
{

}

void tagToEdgeFormatter(std::string pathInputFile, int numberOfThreads, std::string pathOutputFile)
{
    std::thread threads[numberOfThreads];
    std::map<std::string,int> weightsPerThread[numberOfThreads];
    std::set<std::string> edges;
    std::map<std::string, std::set<std::string>> linksMap;
    std::vector<std::string> lines;
    std::vector<std::string> links;
    std::fstream pathFile(pathInputFile);

    std::string tmp;

    while(std::getline(pathFile, tmp))
    {
        lines.insert(lines.end(),tmp);
    }
    pathFile.close();

    for(int i = 0; i < numberOfThreads;i++)
    {
        int start = (int)((float)i/(float)numberOfThreads*lines.size());
        int end = (int)((float)(i+1)/(float)numberOfThreads*lines.size());
        std::cout<<"debug: starting thread "<<i<<" with parameters: begin "<<start<<" end "<<end<<std::endl;
        threads[i] = std::thread(readLinks, i, start, end, std::ref(lines), std::ref(linksMap), std::ref(links));
    }
    for(auto & thread : threads)
    {
        thread.join();
    }
    //Test
    std::ofstream resultFile;
    resultFile.open(pathOutputFile);

    for(auto it = linksMap.begin(); it != linksMap.end(); ++it)
    {
        resultFile<<it->first<<'\n';
        for(auto element : it->second)
        {
            resultFile<<element<<' ';
        }
        resultFile<<"\n\n";
    }
    resultFile.close();
    //EndTest
}





int main(int argc, char *argv[])
{
    int threadCount = 4;
    std::string pathOfResultFile = "foundLinks";
    if(argc == 1)
    {
        printf(stderr, "no InputFile passed!");
        return 1;
    }
    else
    {
        std::string inputPath(argv[1]);
        readLinks(threadCount, inputPath, pathOfResultFile);
    }
}