#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define TSIZE 1048576
#define SEED 1159241
#define HASHFN bitwisehash

static const int MAX_STRING_LENGTH = 1000;
typedef double real;

typedef struct cooccur_rec {
    int word1;
    int word2;
    real val;
} CREC;

typedef struct hashrec {
    char	*word;
    long long id;
    struct hashrec *next;
} HASHREC;

/* Efficient string comparison */
int scmp( char *s1, char *s2 ) {
    while(*s1 != '\0' && *s1 == *s2) {s1++; s2++;}
    return(*s1 - *s2);
}

/* Move-to-front hashing and hash function from Hugh Williams, http://www.seg.rmit.edu.au/code/zwh-ipl/ */

/* Simple bitwise hash function */
unsigned int bitwisehash(char *word, int tsize, unsigned int seed) {
    char c;
    unsigned int h;
    h = seed;
    for(; (c =* word) != '\0'; word++) h ^= ((h << 5) + c + (h >> 2));
    return((unsigned int)((h&0x7fffffff) % tsize));
}

/* Create hash table, initialise pointers to NULL */
HASHREC ** inithashtable() {
    int	i;
    HASHREC **ht;
    ht = (HASHREC **) malloc( sizeof(HASHREC *) * TSIZE );
    for(i = 0; i < TSIZE; i++) ht[i] = (HASHREC *) NULL;
    return(ht);
}

/* Search hash table for given string, return record if found, else NULL */
HASHREC *hashsearch(HASHREC **ht, char *w) {
    HASHREC	*htmp, *hprv;
    unsigned int hval = HASHFN(w, TSIZE, SEED);
    for(hprv = NULL, htmp=ht[hval]; htmp != NULL && scmp(htmp->word, w) != 0; hprv = htmp, htmp = htmp->next);
    if( htmp != NULL && hprv!=NULL ) { // move to front on access
        hprv->next = htmp->next;
        htmp->next = ht[hval];
        ht[hval] = htmp;
    }
    return(htmp);
}

/* Insert string in hash table, check for duplicates which should be absent */
void hashinsert(HASHREC **ht, char *w, long long id) {
    HASHREC	*htmp, *hprv;
    unsigned int hval = HASHFN(w, TSIZE, SEED);
    for(hprv = NULL, htmp = ht[hval]; htmp != NULL && scmp(htmp->word, w) != 0; hprv = htmp, htmp = htmp->next);
    if(htmp == NULL) {
        htmp = (HASHREC *) malloc(sizeof(HASHREC));
        htmp->word = (char *) malloc(strlen(w) + 1);
        strcpy(htmp->word, w);
        htmp->id = id;
        htmp->next = NULL;
        if(hprv == NULL) ht[hval] = htmp;
        else hprv->next = htmp;
    }
    else fprintf(stderr, "Error, duplicate entry located: %s.\n",htmp->word);
    return;
}


int find_arg(char *str, int argc, char **argv) {
    int i;
    for (i = 1; i < argc; i++) {
        if(!scmp(str, argv[i])) {
            if (i == argc - 1) {
                printf("No argument given for %s\n", str);
                exit(1);
            }
            return i;
        }
    }
    return -1;
}


int main(int argc, char **argv)
{
	char *inputFile, *vocabFile, *binFile, *tmpstr1, *tmpstr2;
	FILE *file, *binFileExport;
	CREC tmpElement;
	HASHREC *htmp, **vocab_hash = inithashtable();
	long long j=0, id, counter=0;
	float tmpfloat;
	int i;

	inputFile = malloc(sizeof(char)*MAX_STRING_LENGTH);
	vocabFile = malloc(sizeof(char)*MAX_STRING_LENGTH);
	binFile = malloc(sizeof(char)*MAX_STRING_LENGTH);
	tmpstr1 = malloc(sizeof(char)*MAX_STRING_LENGTH);
	tmpstr2 = malloc(sizeof(char)*MAX_STRING_LENGTH);

	if(argc == 1)
	{
		fprintf(stderr, "No argument passed! Programm canceled\n");
		return 1;
	}

	if ((i = find_arg((char *)"-input", argc, argv)) > 0) strcpy(inputFile, argv[i+1]);
	else
	{
		fprintf(stderr, "No inputArgument passed! Programm canceled\n");
		return 2;
	}
	if ((i = find_arg((char *)"-vocab", argc, argv)) > 0) strcpy(vocabFile, argv[i+1]);
	else
	{
		fprintf(stderr, "No vocabArgument passed! Programm canceled\n");
		return 3;
	}
	file = fopen(vocabFile,"r");

	while(fscanf(file, "%s %lld", tmpstr1, &id) != EOF) 
	{
		hashinsert(vocab_hash, tmpstr1, id);
		j++;
	}

	fclose(file);

	fprintf(stderr, "read in %d entries\n", j);

	sprintf(binFile, "%s.bin", inputFile);
	binFileExport = fopen(binFile,"w");
	file = fopen(inputFile,"r");


	while(fscanf(file, "%s\t%s\t%lf\n", tmpstr1, tmpstr2, &(tmpElement.val)) != EOF)
	{
		tmpElement.word1 = hashsearch(vocab_hash, tmpstr1)->id;
		tmpElement.word2 = hashsearch(vocab_hash, tmpstr2)->id;
                fwrite(&(tmpElement.word1), sizeof(int), 1, binFileExport);
                fwrite(&(tmpElement.word2), sizeof(int), 1, binFileExport);
                fwrite(&(tmpElement.val), sizeof(real), 1, binFileExport);
		/*if(counter % 1000 == 0)
		{
			//printf(stderr, "Writing %s\t%s\t%lf to file \n", tmpstr1, tmpstr2, tmpElement.val);
			printf("%cProgress: %d entries\n", counter);
			fflush(stdout);
		}*/
		counter++;
	}
	fclose(file);
	fclose(binFileExport);

}
