#!/usr/bin/python3

import pandas as pd
import subprocess as sb

from optparse import OptionParser

parser = OptionParser()
parser.add_option("-i",
                  "--input",
                  dest="input",
                  type="string",
                  help="path of edgeFile separated with tabs",
                  default="")

(opts, args) = parser.parse_args()

elements = pd.read_csv(opts.input, sep='\t', names=['element1','element2','relatedness'], dtype={'element1':str, 'element2':str, 'relatedness':float})

print('completed reading tsv-file')

words = set()
index = 1
for word in elements['element1']:
  words.add(word)
for word in elements['element2']:
  words.add(word)

file = open('vocab'+opts.input,'w')
for word in words:
  file.write(str(word)+' '+str(index)+'\n')
  index += 1

file.close()

print('finished writing vocab-file')

sb.call(['gcc','cooccurrenceBinFormatter.c','-o','cBF'])
sb.call(['./cBF','-input',opts.input,'-vocab','vocab'+opts.input])
sb.call(['rm','cBF'])
